#!/usr/bin/env python3
# ==============================================================================
# ECE/CSE 848
# Torcs GA 
# Authors: Kevin Smith (smit2958@msu.edu),
#          Chris Nosowsky (nosowsky@msu.edu)
#
# Classes for the bot driver in torcs. This is a direct Python converstion from 
# the driver found in the torcs_ros package that was written in C++. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
import torcs_ga_interface.Interface as TI
from torcs_ga_msgs.srv import ResetData, ResetDataResponse
from torcs_ga_msgs.msg import DriverParams
import math
import rospy
import numpy as np

# ===[ CONSTANTS ]==============================================================
_GEARDOWN = 0
_GEARUP = 1
_GEARCTRL = [ [0, 2500, 3000, 3000, 3500, 3500],   # Gear Down
              [5000, 6000, 6000, 6500, 7000, 0] ]  # Gear Up


_CLUTCH_MAX = 0.5
_CLUTCH_DELTA = 0.05
_CLUTCH_DELTA_TIME = 0.02
_CLUTCH_DELTA_RACED = 10.0
_CLUTCH_MAX_MODIFIER = 1.3
_CLUTCH_DEC = 0.01

_BRAKE_ABS_MIN_SPEED = 3.0
_WHEEL_RADIUS = [0.3306, 0.3306, 0.3276, 0.3276]
_BRAKE_ABS_SLIP = 2.0
_BRAKE_ABS_RANGE = 3.0

_SPEED_MAX_VALUE = 300.0 #200      # 150    
_SPEED_MAX_DISTANCE = 200.0 #100.0     # 70

_STEER_SENSITIVITY_OFFSET = 80.0
_STEER_LOCK = 0.366519
_WHEEL_SENSITIVITY_COEFF = 1.0

_STUCK_ANGLE = math.pi/6 # 0.523598775
_STUCK_TIME = 25

# Chris Mod
TARGET_SPEED_RULE_SET = [280, 240, 220, 180, 120, 60, 30]
# TARGET_SPEED_RULE_SET = [70, 60, 50, 40, 30, 20, 10]
TARGET_STEER_RULE_SET = [0, 0.25, 0.5, 1]

# ===[ CLASSES ]================================================================
class TORCS_Driver(object):
    def __init__(self, PortId=3001):
        self.PortId = PortId

        # ROS communication services
        self.UpdateParams = rospy.Subscriber('/torcs_ros{}/DriverParams'.format(self.PortId-3000), DriverParams, self._cb_UpdateParamData)
        self.ResetDataSrv = rospy.Service('/torcs_ros{}/ResetData'.format(self.PortId-3000), ResetData, self._srvResetRace)
        
        # Classes for sensor data 
        self.Ctrl = TI.Ctrl_Cmd(self.PortId)
        self.Sensors = TI.Sensors_State(self.PortId)
        self.Track = TI.ScanTrack(self.PortId)
        self.Speed = TI.Speed(self.PortId)
        
        self.Rate = rospy.Rate(50)
        self.stuck = 0

        # self.mfFront = [0, 50, 20, 80, 60, 100]
        # self.mfM5 = [0, 40, 10, 70, 50, 100]
        # self.mfM10 = [0, 30, 20, 60, 50, 100]
        self.mfFront = [0, 100, 100, 160, 120, 200]
        self.mfM5 = [0, 80, 80, 140, 100, 200]
        self.mfM10 = [0, 60, 60, 120, 100, 200]
        self.logIt = True
            
    def _degToRad(self, degrees):
        return float((float(degrees) * float((math.pi/180))))

    def _getPortId(self):
        return self.PortId - 3000

    def _srvResetRace(self, request):
        rospy.logdebug( "Node: {} resetting data".format(self._getPortId()) )
        rsp = ResetDataResponse()
        self.Sensors.reset()
        rsp.Ack = 1
        return rsp

    def _cb_UpdateParamData(self, data):
        for index, item in enumerate(data.newParams):
            if index < 6:
                self.mfFront[index] = item
            if index >= 6 and index < 12:
                self.mfM5[index-6] = item
            if index >= 12:
                self.mfM10[index-12] = item
        rospy.logdebug("Node: {} received data {} {} {} ".format(self.PortId, self.mfFront, self.mfM5, self.mfM10))


    def masterTrapezoidalFunction(self, sensor, params, n=3):
        # LOW Trapezoid
        low = -999
        if params[0] <= sensor <= params[1]:
            low = 1
        elif params[1] <= sensor <= params[2]:
            try:
                low = (params[2] - sensor) / (params[2] - params[1])
            except ZeroDivisionError:
                low = 0
        elif sensor > params[2]:
            low = 0

        # MEDIUM Trapezoid
        i = 2
        med = -999
        if sensor <= params[(2*i)-3]:
            med = 0
        if params[(2*i)-3] <= sensor <= params[(2*i)-2]:
            try:
                med = (sensor - params[(2*i)-3]) / (params[(2*i)-2] - params[(2*i)-3])
            except ZeroDivisionError:
                med = 1
        if params[(2*i)-2] <= sensor <= params[(2*i)-1]:
            med = 1
        if params[(2*i)-1] <= sensor <= params[(2*i)]:
            try:
                med = (params[(2*i)] - sensor) / (params[(2*i)] - params[(2*i)-1])
            except ZeroDivisionError:
                med = 0
        if sensor > params[(2*i)]:
            med = 0   


        # HIGH Trapezoid
        high = -999
        if sensor <= params[(2*n)-3]:
            high = 0
        if params[(2*n)-3] <= sensor <= params[(2*n)-2]:
            try:
                high = (sensor - params[(2*n)-3]) / (params[(2*n)-2] - params[(2*n)-3])
            except ZeroDivisionError:
                high = 1
        if sensor > params[(2*n)-2]:
            high = 1

        lowRes = False
        medRes = False
        highRes = False
        if high >= 0.5:
            highRes = True
        elif med >= 0.5:
            medRes = True
        elif low >= 0.5:
            lowRes = True
        

        return lowRes, medRes, highRes

    def speedController(self):
        if ( (self.Sensors.MsgData.trackPos < 1) and (self.Sensors.MsgData.trackPos > -1) ):

            # Send the current speed to the sensor state class to track max speed as this  
            # information is not available to us otherwise. 
            self.Sensors.updateMaxSpeed(self.Speed.MsgData.twist.linear.x)

            targetSpeed = 0

            m5Sensor = max(self.Track.MsgData.ranges[9], self.Track.MsgData.ranges[11])
            frontSensor = self.Track.MsgData.ranges[10]
            m10Sensor = max(self.Track.MsgData.ranges[8], self.Track.MsgData.ranges[12])
            # First setup 1 master trapezoid functions (combined of 3 trapezoid functions)
            # Call 1 for each sensor (3 calls)
            # Tuple return Low, Med, High
            frontLow, frontMedium, frontHigh = self.masterTrapezoidalFunction(frontSensor, self.mfFront)
            # frontLow = self.mfFront[0] <= frontSensor <= self.mfFront[1]
            # frontMedium = self.mfFront[2] <= frontSensor <= self.mfFront[3]
            # frontHigh = self.mfFront[4] <= frontSensor <= self.mfFront[5]

            m5Low, m5Medium, m5High = self.masterTrapezoidalFunction(m5Sensor, self.mfM5)
            # m5Low = self.mfM5[0] <= m5Sensor <= self.mfM5[1]
            # m5Medium = self.mfM5[2] <= m5Sensor <= self.mfM5[3]
            # m5High = self.mfM5[4] <= m5Sensor <= self.mfM5[5]

            m10Low, m10Medium, m10High = self.masterTrapezoidalFunction(m10Sensor, self.mfM10)
            # m10Low = self.mfM10[0] <= m10Sensor <= self.mfM10[1]
            # m10Medium = self.mfM10[2] <= m10Sensor <= self.mfM10[3]
            # m10High = self.mfM10[4] <= m10Sensor <= self.mfM10[5]

            frontMax = self.mfFront[5] <= frontSensor <= _SPEED_MAX_DISTANCE
            m5Max = self.mfM5[5] <= m5Sensor <= _SPEED_MAX_DISTANCE
            m10Max = self.mfM10[5] <= m10Sensor <= _SPEED_MAX_DISTANCE

            if frontHigh:
                targetSpeed = TARGET_SPEED_RULE_SET[0]
            if frontMedium:
                targetSpeed = TARGET_SPEED_RULE_SET[1]
            if (frontLow) and (m5High):
                targetSpeed = TARGET_SPEED_RULE_SET[2]
            if (frontLow) and (m5Medium):
                targetSpeed = TARGET_SPEED_RULE_SET[3]
            if (frontLow) and (m5Low) and (m10High):
                targetSpeed = TARGET_SPEED_RULE_SET[4]
            if (frontLow) and (m5Low) and (m10Medium):
                targetSpeed = TARGET_SPEED_RULE_SET[5]
            if (frontLow) and (m5Low) and (m10Low):
                targetSpeed = TARGET_SPEED_RULE_SET[6]
            if (frontMax) or (m5Max) or (m10Max):
                targetSpeed = _SPEED_MAX_VALUE

            return 2 / (1 + math.exp(self.Speed.MsgData.twist.linear.x - targetSpeed)) - 1
        else:
            targetSpeed = 0.3
            return targetSpeed

    def steerController(self):
        steer = 0
        if len(self.Track.MsgData.ranges) > 0:
            m5Sensor = max(self.Track.MsgData.ranges[9], self.Track.MsgData.ranges[11])
            frontSensor = self.Track.MsgData.ranges[10]
            m10Sensor = max(self.Track.MsgData.ranges[8], self.Track.MsgData.ranges[12])
            
            frontLow, frontMedium, frontHigh = self.masterTrapezoidalFunction(frontSensor, self.mfFront)
            # frontLow = self.mfFront[0] <= frontSensor <= self.mfFront[1]
            # frontMedium = self.mfFront[2] <= frontSensor <= self.mfFront[3]
            # frontHigh = self.mfFront[4] <= frontSensor <= self.mfFront[5]

            m5Low, m5Medium, m5High = self.masterTrapezoidalFunction(m5Sensor, self.mfM5)
            # m5Low = self.mfM5[0] <= m5Sensor <= self.mfM5[1]
            # m5Medium = self.mfM5[2] <= m5Sensor <= self.mfM5[3]

            m10Low, m10Medium, m10High = self.masterTrapezoidalFunction(m10Sensor, self.mfM10)
            # m10Medium = self.mfM10[2] <= m10Sensor <= self.mfM10[3]
            # m10High = self.mfM10[4] <= m10Sensor <= self.mfM10[5]

            frontMax = 150 <= frontSensor <= _SPEED_MAX_DISTANCE

            if frontHigh:
                steer = TARGET_STEER_RULE_SET[0]
            if (frontMedium) and (m10High):
                steer = TARGET_STEER_RULE_SET[1]
            if (frontMedium) and (m10Medium) and (m5Medium):
                steer = TARGET_STEER_RULE_SET[1]
            if (frontMedium) and (m10Medium) and (m5Low):
                steer = TARGET_STEER_RULE_SET[2]
            if (frontLow) and (m10High):
                steer = TARGET_STEER_RULE_SET[2]
            if (frontLow) and (m10Medium) and (m5Medium):
                steer = TARGET_STEER_RULE_SET[3]
            if (frontLow) and (m10Medium) and (m5Low):
                steer = TARGET_STEER_RULE_SET[3]

            if m10Sensor == self.Track.MsgData.ranges[8]:
                steer = -steer

            # For straightways below. Idea is to create a good arc into corner and not go out of bounds!
            if (frontMax) and self.Sensors.MsgData.trackPos > 0:    # Straightway but on left side. Should be on right to get better arc.
                steer = -0.05   # Steer right
            if frontSensor == -1.0 or self.Sensors.MsgData.trackPos < -0.9: # Off track or about to be, make bigger steer correction.
                steer = 0.1     # Steer left
            if ((frontMax)) and (-0.9 >= self.Sensors.MsgData.trackPos <= -0.80) \
            and self.Sensors.MsgData.angle > 0: # Straightway on outside edge, tracking out.
                steer = 0.05
            if (frontMax) and (-0.9 >= self.Sensors.MsgData.trackPos <= -0.70) \
            and self.Sensors.MsgData.angle < 0: # Straightway and on outside edge, tracking in.
                steer = -0.1
            if self.Sensors.MsgData.trackPos >= 0.80:   # Far left side of track, correct it back to middle
                steer = -0.1
        return steer

    def acceleration(self):
        if ( (self.Sensors.MsgData.trackPos < 1) and (self.Sensors.MsgData.trackPos > -1) ):
            rightSensor = self.Track.MsgData.ranges[9]
            centerSensor = self.Track.MsgData.ranges[10]
            leftSensor = self.Track.MsgData.ranges[11]

            targetSpeed = 0

            # Track is straight. Max speed! 
            if ( (centerSensor > _SPEED_MAX_DISTANCE) or 
                 ( (centerSensor >= rightSensor) and (centerSensor >= leftSensor)) ):
                targetSpeed = _SPEED_MAX_VALUE
            else:
                # Right hand turn
                if (rightSensor > leftSensor):
                    h = centerSensor * math.sin(self._degToRad(5))
                    b = rightSensor - centerSensor * math.cos(self._degToRad(5))
                    sinAngle = (b * b)/( (h*h) + (b*b) )
                    targetSpeed = _SPEED_MAX_VALUE * ( (centerSensor * sinAngle) / _SPEED_MAX_DISTANCE )
                else:
                    # Left hand turn
                    h = centerSensor * math.sin(self._degToRad(5))
                    b = leftSensor - centerSensor * math.cos(self._degToRad(5))
                    sinAngle = (b * b)/( (h*h) + (b*b) )
                    targetSpeed = _SPEED_MAX_VALUE * ( (centerSensor * sinAngle) / _SPEED_MAX_DISTANCE )
    
            # accel/brake command is expontially scaled w.r.t. the difference between target speed and current one
            return 2 / (1 + math.exp(self.Speed.MsgData.twist.linear.x - targetSpeed)) - 1
        else:
            # When off the track, return a moderate accleeration command
            return 0.3

    def brakeCtrl(self, brake):
        # convert to m/s
        currentSpeed = self.Speed.MsgData.twist.linear.x / 3.6

        if currentSpeed < _BRAKE_ABS_MIN_SPEED:
            return brake

        slip = 0.0
        for index, sensor in enumerate(self.Sensors.MsgData.wheelSpinVel):
            slip += sensor * _WHEEL_RADIUS[index]
        slip = currentSpeed - slip/4.0

        # If slip too high, apply ABS
        if (slip > _BRAKE_ABS_SLIP):
            brake = brake - (slip - _BRAKE_ABS_SLIP) / _BRAKE_ABS_RANGE
        
        # Brake cannot go negative, so limit to 0 or sent the brake calculation 
        return max(0.0, brake)


    def clutchCtrl(self):
        """" Calculates the new clutch value from the current clutch in the control message """
        maxClutch = _CLUTCH_MAX
        delta = _CLUTCH_DELTA

        if ( (self.Sensors.MsgData.currentLapTime < _CLUTCH_DELTA_TIME) and
             (self.Sensors.MsgData.distRaced < _CLUTCH_DELTA_RACED) ) :
             self.Ctrl.MsgData.clutch = _CLUTCH_MAX

        if (self.Ctrl.MsgData.clutch > 0):
            if (self.Ctrl.MsgData.gear < 2):
                delta = _CLUTCH_DELTA / 2
                maxClutch *= _CLUTCH_MAX_MODIFIER
                if (self.Sensors.MsgData.currentLapTime < _CLUTCH_DELTA_TIME):
                    self.Ctrl.MsgData.clutch = maxClutch
            
            # Limit it back to max value
            self.Ctrl.MsgData.clutch = min(maxClutch, self.Ctrl.MsgData.clutch)

            if (self.Ctrl.MsgData.clutch != maxClutch):
                self.Ctrl.MsgData.clutch -= delta
                self.Ctrl.MsgData.clutch = max(0.0, self.Ctrl.MsgData.clutch)   
            else:
                self.Ctrl.MsgData.clutch = _CLUTCH_DEC

    def gearCtrl(self):
        # -1 (R), 0 (N), 1-6 (Drive)
        # When the race ends, the client disconnects and causes the gear to go out of range.
        currentGear = np.clip(self.Sensors.MsgData.gear, -1, 6)
        currentRPM = self.Sensors.MsgData.rpm
        newGear = currentGear 

        if (currentGear < 1):
            newGear = 1
        
        if (currentGear < 6 and currentRPM >= _GEARCTRL[_GEARUP][(currentGear - 1)] ):
            newGear += 1
        elif (currentGear > 1 and currentRPM <= _GEARCTRL[_GEARDOWN][(currentGear - 1)] ):
            newGear -= 1
        else:
            pass # Do nothing

        newGear = min(max(newGear, -1), 6)
    
        return newGear
        
    def steering(self):
        # Steering angle is computed by correcting the actual car angle w.r.t. to track
        # axis and to adjust car position w.r.t to middle of track 
        targetAngle = self.Sensors.MsgData.angle - (self.Sensors.MsgData.trackPos * 0.5)

        if (self.Speed.MsgData.twist.linear.x > _STEER_SENSITIVITY_OFFSET):
            return targetAngle/ (_STEER_LOCK * (self.Speed.MsgData.twist.linear.x - _STEER_SENSITIVITY_OFFSET) * _WHEEL_SENSITIVITY_COEFF)
        else:
            return targetAngle / _STEER_LOCK

       
    def drive(self):
        if ( abs(self.Sensors.MsgData.angle) > _STUCK_ANGLE):
            self.stuck += 1
        else:
            self.stuck = 0 # Not stuck, reset counter.

        # Move reset flag between new of the sensors
        self.Sensors.RestartActive = self.Ctrl.RestartActive

        
        if (self.stuck > _STUCK_TIME):
            # Assume we are pointing away from the track
            steer = -self.Sensors.MsgData.angle / _STEER_LOCK
            gear = -1 # Reverse

            # if we are facing the track, go forward instead
            if (self.Sensors.MsgData.angle * self.Sensors.MsgData.trackPos > 0):
                gear = 1
                steer = -steer

            self.clutchCtrl()

            # Update control messsage
            self.Ctrl.MsgData.header.stamp = rospy.Time.now()
            self.Ctrl.MsgData.accel = 1.0
            self.Ctrl.MsgData.brake = 0.0
            self.Ctrl.MsgData.gear = gear
            self.Ctrl.MsgData.steering = steer
            #self.Ctrl.MsgData.clutch is already update by clutchCtrl 

            # Send message
            self.Ctrl.Publish()
        else: 
            # Car is not stuck! 
            if (len(self.Track.MsgData.ranges) > 0):
                # accelAndBrake = self.acceleration() # OLD WAY BEFORE FUZZY CONTROLLER
                accelAndBrake = self.speedController()  # Chris - New way
            else:
                accelAndBrake = 0

            gear = self.gearCtrl()
            # steer = np.clip(self.steering(), -1, 1) # Limit the value to between -1 and 1 
            steer = np.clip(self.steerController(), -1, 1)  # Chris - New way
            if (accelAndBrake > 0):
                acceleration = accelAndBrake
                brake = 0
            else:
                acceleration = 0
                brake = self.brakeCtrl(-accelAndBrake)

            self.clutchCtrl()

            self.Ctrl.MsgData.header.stamp = rospy.Time.now()
            self.Ctrl.MsgData.accel = acceleration
            self.Ctrl.MsgData.brake = brake
            self.Ctrl.MsgData.gear = gear
            self.Ctrl.MsgData.steering = steer
            #self.Ctrl.MsgData.clutch is already update by clutchCtrl 
            self.Ctrl.Publish()

    def execute(self):
        while True and not rospy.is_shutdown():
            self.drive()
            self.Rate.sleep()

# ===[ EOF ]====================================================================
