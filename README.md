# Finding an Optimal Racing Line with TORCS and ROS
ECE/CSE 848 end of semester project. This project executes a ROS node that runs a genetic algorithm to control a TORCS vehicle in a simulated race to determine the best values for the fuzzy logic speed and steering controllers.  

## Installation
### ROS
The version of ROS was the Noetic Ninjemy's version. Installation steps were followed from the following page: 

http://wiki.ros.org/noetic/Installation

### TORCS
Since we used Ubuntu 20.04 we followed the installation instructions at the following website: 
https://github.com/fmirus/torcs-1.3.7

### Opencv
Can be installed by running: 

```pip3 install opencv-python```

### Pymoo
```pip3 install pymoo```

### torcs_ga
This project should be cloned into the ```catkin_ws``` directory that was created during the ROS installation. Once the code has been downloaded a ```catkin_make``` needs to be performed before running the nodes. Please refer to the ROS website for details on how to build packages. 

TORCS should be started and configured to run a practice or quick race. The number of drivers and laps can be configured in the on screen menus. 

A terminal window should be opened and ROS needs to be launch by running: 

```$roscore```

Another terminal window can be opened to launc the TORCS GA and driver nodes by the following command: 

```$roslaunch torcs_ga torcs_ga.launch Drivers:=10```

Drivers can be set to any value between 1 and 10. If omitted, 1 is the default value. The nodes will start to connect to TORCS and the evaluation will begin. Once completed, the terminal will report the best solution and fitness score. 
