#!/usr/bin/env python3
# ==============================================================================
# ECE/CSE 848
# Torcs GA 
# Authors: Kevin Smith (smit2958@msu.edu),
#          Chris Nosowsky (nosowsky@msu.edu)
#
# Classes to interact with the TORCS ROS nodes
# ==============================================================================

# ===[ IMPORTS ]================================================================
from sys import setrecursionlimit
import rospy
#from torcs_ga.GA import TORCS_GA
from torcs_msgs.msg import TORCSCtrl, TORCSSensors
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import TwistStamped
from torcs_ga_msgs.srv import DriverRaceData, DriverRaceDataRequest
from torcs_ga_msgs.msg import RestartRace, LapStatus


# ===[ CONSTANTS ]==============================================================
_MAX_LAP_COUNT = 1

# ===[ CLASSES ]================================================================
class TopicsSuperClass(object):
	""" Super class that is shared with all the other TORCS ROS classes """
	def __init__(self, PortId=3001):
		self.Pub = None
		self.Sub = None
		self.MsgData = None
		self.PortId = self._determinePortId(PortId)
		self.RestartActive = False
		self.PrevRestartActive = False

	def _determinePortId(self, PortId):
		return (PortId - 3000)

	def publish(self, data=None):
		""" Publishes the data into the ROS network. If no data is provided then
		 	the MsgData value is used. 
		"""
		if data == None:
			data = self.MsgData
		
		if self.Pub != None and data != None:
			self.Pub.publish(data)
		else:
			rospy.logwarn("Node does not have a publisher configured")


class Ctrl_Cmd(TopicsSuperClass):
	""" Handles the Ctrl_Cmd Torcs messages. These are both receieved and sent by this node. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/ctrl_cmd".format(self.PortId), TORCSCtrl, callback=self._cb_RxCtrlCmdMsg)
		self.Pub = rospy.Publisher("/torcs_ros{}/ctrl_cmd".format(self.PortId), TORCSCtrl, queue_size=3)
		self.RestartSub = rospy.Subscriber("/torcs_ga_alg/RestartRace".format(self.PortId), RestartRace, self._srvRestartRace)
		self.MsgData = TORCSCtrl()

	def _srvRestartRace(self, data):
		self.RestartActive = data.mode

	def Publish(self):
		""" Update the message data and prepare to publish """
		if (self.RestartActive == True and self.PrevRestartActive == False):
			#if (self.PortId == 1):
			self.MsgData.meta = 1
			self.PrevRestartActive == True
			#else:
			#	self.MsgData.meta = 0
		elif (self.RestartActive == False):
			self.PrevRestartActive = False
			self.MsgData.meta = 0
		
		
		'''
		if (self.RestartActive == True) and (self.PrevRestartActive == False):
			# Hold was just enabled.
			self.PrevRestartActive = True
			rospy.logdebug("Hold active")
		elif (self.RestartActive == False) and (self.PrevRestartActive == True):
			# Hold was clear, have driver 1 send the restart command
			rospy.logdebug("Hold Removed")
			if (self.PortId == 1):
				rospy.logdebug("Setting META to restart...")
				self.MsgData.meta = 1
				rospy.logdebug("Node {}: Restarting race...".format(self.PortId))
			else:
				self.MsgData.meta = 0
			self.PrevRestartActive = False
		else:
			self.MsgData.meta = 0
		'''

		self.Pub.publish(self.MsgData)

	def _cb_RxCtrlCmdMsg(self, msg):
		"""
			Header header
			float64 accel 		# Virtual gas pedal (0 means no gas, 1 full gas)
			float64 brake		# Virtual brake pedal (0 means no brake, 1 full brake)
			float64 clutch 		# Virtual clutch pedal (0 means no clutch, 1 full clutch)
			uint8 gear 			# Gear value: -1,0,1,...,6 where -1 means backwards driving and 0 means idle
			float64 steering	# Steering value: -1 and +1 means respectively full right and left, that 
								# corresponds to an angle of 0.366519 rad
			float64 focus		# Focus direction for the focus sensors in degrees. Currently unused
			uint8 meta			# This is meta-control command: 0 do nothing, 1 ask compe-
								# tition server to restart the race. Currently unused
		"""
		self.MsgData = msg

class ScanOpponents(TopicsSuperClass):
	""" Handles the Scan Opponents Torcs message. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/scan_opponents".format(self.PortId), LaserScan, callback=self._cb_RxScanOpponents)
		self.MsgData = LaserScan()

	def _cb_RxScanOpponents(self, msg):
		"""
			Header header		    # timestamp in the header is the acquisition time of the first ray in the scan.
		     	            		# in frame frame_id, angles are measured around the positive Z axis (counterclockwise, 
									# if Z is up) with zero angle being forward along the x axis
			float32 angle_min       # start angle of the scan [rad]
			float32 angle_max   	# end angle of the scan [rad]"
			float32 angle_increment # angular distance between measurements [rad]
			time_increment   		# time between measurements [seconds] - if your scanner
			                        # is moving, this will be used in interpolating position
			                        # of 3d points
			float32 scan_time       # time between scans [seconds]
			float32 range_min       # minimum range value [m]
			float32 range_max       # maximum range value [m]
			float32[36] ranges      # range data [m] (Note: values < range_min or > range_max should be discarded)
			float32[] intensities   # intensity data [device-specific units].  If your device does not provide 
									# intensities, please leave the array empty.
			
			# Note, max angle is 3.11, min angle -pi, increment is 0.1736602634191513, range max 200. 
		"""
		self.MsgData = msg

class Sensors_State(TopicsSuperClass):
	""" Handles the Sensor State Torcs message. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/sensors_state".format(self.PortId), TORCSSensors, callback=self._cb_RxSensorsStateMsg)
		self.LapStatusPub = rospy.Publisher("/torcs_ros{}/LapStatus".format(self.PortId), LapStatus, queue_size=3)
		self.MsgData = TORCSSensors()
		
		self.LapCount = 1
		self.LapTimes = []
		self.maxSpeed = 0
		self.dataSent = False 
		self.lastLapTime = 0

	def updateMaxSpeed(self, currentSpeed):
		if currentSpeed > self.maxSpeed:
			self.maxSpeed = currentSpeed

	def reset(self):
		# Reset key variables when the race is restarted 
		self.LapCount = 1
		self.LapTimes.clear()
		self.maxSpeed = 0
		self.dataSent = False
	
	def _cb_RxSensorsStateMsg(self, msg):
		""" 
			The callback functions moves the data received to the class's internal variable to be processed later. 
			Provides the following sensor information 
			std_msgs/Header header
			float64 angle 			# Angle between the car direction and the direction of the track axis [-PI,PI] in rad
			float64 currentLapTime  # Time elapsed during current lap in seconds
			float64 damage 			# Current damage of the car (the higher is the value the higher is the damage)
			float64 distFromStart 	# Distance of the car from the start line along the track line
			float64 distRaced 		# Distance covered by the car from the beginning of the race
			float64 fuel 			# Current fuel level
			uint8 gear 				# Current gear: -1 is reverse, 0 is neutral and the gear from 1 to 6
			float64 lastLapTime		# Time to complete the last lap
			uint8 racePos 			# Position in the race with respect to other cars (KS: Modified this to be the current lap for the vehicle)
			float64 rpm 			# Number of rotation per minute of the car engine

			# Distance between the car and the track axis. The value is
			# normalized w.r.t to the track width: it is 0 when car is on
			# the axis, -1 when the car is on the right edge of the track
			# and +1 when it is on the left edge of the car. Values greater
			# than 1 or smaller than -1 mean that the car is outside of
			# the track.
			float64 trackPos

			float64[] wheelSpinVel 	# Vector of 4 sensors representing the rotation speed of wheels in radians
			float64 z 				# Distance of the car mass center from the surface of the track along the Z axis in meters
		"""
		self.MsgData = msg

		if (self.RestartActive == True) and (self.PrevRestartActive == False):
			# Hold was just enabled.
			self.PrevRestartActive = True
			self.reset()
		elif (self.RestartActive == False) and (self.PrevRestartActive == True):
			# Hold was clear, have driver 1 send the restart command
			self.PrevRestartActive = False

		self.LapCount = self.MsgData.racePos
		# Send data to GA 
		if (self.RestartActive != True):
			data = LapStatus()
			data.nodeId = self.PortId
			if (self.lastLapTime != self.MsgData.lastLapTime) and (self.MsgData.lastLapTime > 10):
				self.LapTimes.append(self.MsgData.lastLapTime)
				self.lastLapTime = self.MsgData.lastLapTime
				data.lapTimes = self.LapTimes 
			else:
				data.lapTimes = self.LapTimes

			data.damage = self.MsgData.damage
			data.maxSpeed = self.maxSpeed
			if (self.LapCount > 0):
				data.lapsCompleted = self.LapCount - 1 # Since the racePos will have incremented, remove it. 
			else:
				data.lapsCompleted = 0
			self.LapStatusPub.publish(data)

class ScanTrack(TopicsSuperClass):
	""" Handles the Scan Track Torcs message. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/scan_track".format(self.PortId), LaserScan, callback=self._cb_RxScanTrackMsg)
		self.MsgData = LaserScan()

	def _cb_RxScanTrackMsg(self, msg):
		"""
			Header header		    # timestamp in the header is the acquisition time of the first ray in the scan.
		     	            		# in frame frame_id, angles are measured around the positive Z axis (counterclockwise, 
									# if Z is up) with zero angle being forward along the x axis
			float32 angle_min       # start angle of the scan [rad]
			float32 angle_max   	# end angle of the scan [rad]"
			float32 angle_increment # angular distance between measurements [rad]
			time_increment   		# time between measurements [seconds] - if your scanner
			                        # is moving, this will be used in interpolating position
			                        # of 3d points
			float32 scan_time       # time between scans [seconds]
			float32 range_min       # minimum range value [m]
			float32 range_max       # maximum range value [m]
			float32[19] ranges      # range data [m] (Note: values < range_min or > range_max should be discarded)
			float32[] intensities   # intensity data [device-specific units].  If your device does not provide 
									# intensities, please leave the array empty.

			# Note, max angle is pi, min angle -pi, increment is 0.16534698009490967, range max 200. 
			# Index 0 is left side of the vehicle, 18 is the right side, with index 9 being in the middle of the vehicle
		"""
		self.MsgData = msg

class ScanFocus(TopicsSuperClass):
	""" Handles the Scan Focus Torcs message. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/scan_focus".format(self.PortId), LaserScan, callback=self._cb_RxScanFocusMsg)
		self.MsgData = LaserScan()

	def _cb_RxScanFocusMsg(self, msg):
		"""
			Header header		    # timestamp in the header is the acquisition time of the first ray in the scan.
		     	            		# in frame frame_id, angles are measured around the positive Z axis (counterclockwise, 
									# if Z is up) with zero angle being forward along the x axis
			float32 angle_min       # start angle of the scan [rad]
			float32 angle_max   	# end angle of the scan [rad]"
			float32 angle_increment # angular distance between measurements [rad]
			time_increment   		# time between measurements [seconds] - if your scanner
			                        # is moving, this will be used in interpolating position
			                        # of 3d points
			float32 scan_time       # time between scans [seconds]
			float32 range_min       # minimum range value [m]
			float32 range_max       # maximum range value [m]
			float32[19] ranges      # range data [m] (Note: values < range_min or > range_max should be discarded)
			float32[] intensities   # intensity data [device-specific units].  If your device does not provide 
									# intensities, please leave the array empty.

			# Note, max angle is 0.01745329238474369, min angle -0.01745329238474369, increment is 0.006981316953897476, 
			# range max 200. 
		"""
		self.MsgData = msg		

class Speed(TopicsSuperClass):
	""" Handles the Speed Torcs message. """
	def __init__(self, PortId):
		super().__init__(PortId)
		self.Sub = rospy.Subscriber("/torcs_ros{}/speed".format(self.PortId), TwistStamped, callback=self._cb_RxSpeedMsg)
		self.MsgData = TwistStamped()

	def _cb_RxSpeedMsg(self, msg):
		"""
			std_msgs/Header				# Standard metadata for higher-level stamped data types.
										# This is generally used to communicate timestamped data
										# in a particular coordinate frame.
			uint32 seq					# Consecutively increasing ID
			time stamp					# Two-integer timestamp that is expressed as: 
										#  * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
										#  * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
										# time-handling sugar is provided by the client library
			string frame_id				# Frame this data is associated with
			geometry_msgs/Twist			# This expresses velocity in free space broken into its linear and angular parts.
										#  * Vector3  linear
				geometry_msgs/Vector3   # This represents a vector in free space. It is only meant to represent a direction. Therefore, it 
										# does not make sense to apply a translation to it (e.g., when applying a generic rigid 
										# transformation to a Vector3, tf2 will only apply the rotation). If you want your 
										# data to be translatable too, use the geometry_msgs/Point message instead.
					float64 x
					float64 y
					loat64 z
										#  * Vector3  angular
				geometry_msgs/Vector3   # This represents a vector in free space. It is only meant to represent a direction. Therefore, it 
										# does not make sense to apply a translation to it (e.g., when applying a generic rigid 
										# transformation to a Vector3, tf2 will only apply the rotation). If you want your 
										# data to be translatable too, use the geometry_msgs/Point message instead.
					float64 x
					float64 y
					loat64 z
		"""
		self.MsgData = msg

# ===[ EOF ]====================================================================
