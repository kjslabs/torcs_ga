#!/usr/bin/env python3
# ==============================================================================
# ECE/CSE 848
# Torcs GA 
# Authors: Kevin Smith (smit2958@msu.edu),
#          Chris Nosowsky (nosowsky@msu.edu)
#
# This file is called by catkin make to ensure the package and the include
# folder is included when the code is executed.
#
# IMPORTANT: Do NOT manually invoke this file. Use the command catkin_make
# instead.
# ==============================================================================

# ===[ IMPORTS ]================================================================

# File is intentially left blank

# ===[ EOF ]====================================================================
