#!/usr/bin/env python3
# ==============================================================================
# ECE/CSE 848
# Torcs GA 
# Authors: Kevin Smith (smit2958@msu.edu),
#          Chris Nosowsky (nosowsky@msu.edu)
#
# Classes to interact with the TORCS ROS nodes
# ==============================================================================

# ===[ IMPORTS ]================================================================
import statistics
import numpy as np
import time
import rospy
import os
import sys
import random
from pymoo.algorithms.soo.nonconvex.ga import GA
from pymoo.factory import get_mutation, get_crossover, get_termination, get_sampling, get_selection
from pymoo.core.problem import ElementwiseProblem
from pymoo.optimize import minimize
from pymoo.core.repair import Repair
from pymoo.interface import mutation
from torcs_ga_msgs.msg import RestartRace, LapStatus, DriverParams
from random import randrange
from datetime import datetime

# ===[ CONSTANTS ]==============================================================
NUM_LAPS = 3        # LAP CONSTANT

# ===[ GLOBALS ]================================================================
RestartRacePub = None
DriverData = []

# ===[ CLASSES ]================================================================
class GADriver:
    def __init__(self, id):
        self.id = id            # ID of the driver to distinguish between each driver
        self.damage = 0         # Damage driver has received in race
        self.lapTimes = []      # Laptimes of driver in race
        self.topSpeed = 0       # Top speed the driver reached in race
        self.lapsCompleted = 0
        self.LapStatusSub = rospy.Subscriber('/torcs_ros{}/LapStatus'.format(self.id), LapStatus, callback=self._cb_LapStatusMsg)
        self.DriverParamsPub = rospy.Publisher('/torcs_ros{}/DriverParams'.format(self.id), DriverParams, queue_size=3)

    def reset(self):
        self.damage = 0
        self.lapTimes = []
        self.topSpeed = 0
        self.lapsCompleted = 0

    def sendUpdatedParams(self, params):
        rospy.logdebug("Sent {} the following params {}".format(self.id, params))
        self.DriverParamsPub.publish(params)
    
    def _cb_LapStatusMsg(self, data):
        self.damage = data.damage
        self.lapTimes = data.lapTimes
        self.topSpeed = data.maxSpeed
        self.lapsCompleted = data.lapsCompleted

    def getDriverID(self):
        return self.id

    def setDriverID(self, id):
        self.id = id

    def getDriverDamage(self):
        return self.damage
    
    def setDriverDamage(self, damage):
        self.damage = damage

    def getBestDriverLapTime(self, index):
        return min(self.lapTimes)    

    def getDriverLapTimes(self):
        return self.lapTimes
    
    def setDriverLapTimes(self, lapTimes):
        self.lapTimes = lapTimes
    
    def getDriverTopSpeed(self):
        return self.topSpeed
    
    def setDriverTopSpeed(self, speed):
        self.topSpeed = speed

class GARepair(Repair):
    def __init__(self):
        super().__init__()
        self.FirstRun = True
        #self.DriverData = Drivers # Needed to get the pubisher access.

    def _do(self, problem, pop, **kwargs):
        global DriverData
        Z = pop.get("X")
        
        # This is only if it is required, this will delay the function
        # a bit on the first run to make sure the other nodes can commmunicate
        # with each other, so far this is not needed. 
        if self.FirstRun == True:
            rospy.sleep(0.1) # Adjust as needed
            self.FirstRun = False
        
        # When only 1 pop size is selected, the repair function has two members after the 
        # first run. My assumption is so that it can 'mate', so we should use the driver 
        # data class instead of the indexing. 
        for index, member in enumerate(Z):
            if index <= len(DriverData):
                front = member[0:6]
                m5 = member[6:12]
                m10 = member[12:18]
                sensors = [front, m5, m10]
                newMember = []
                for sensor in sensors:
                    # Set the limits
                    sensor[0] = 0
                    sensor[5] = 200
                    relativeMin = sensor[0]
                    for i in range(1, len(sensor[1:])):
                        if sensor[i-1] <= sensor[i]: # First time, it will be a comparison with zero.
                            if sensor[i] <= sensor[i+1] and sensor[i] <= 200:
                                relativeMin = sensor[i] # Nothing needs to change in the sensor value, but move the relative min up.
                            else:
                                if sensor[i] >= sensor[5]: # Value is out of range so set it to max 
                                    sensor[i] = 200
                                elif sensor[i] >= sensor[i-1]: # and sensor[i-1] >= sensor[i+1]
                                    sensor[i+1] = random.randint(sensor[i], 200) # Fix the next sensor value to be the current or higher
                                else:
                                    continue
                        else:
                            # First run, this would mean that we have a negative number, which cannot happen so assign it to zero.
                            if i == 1:
                                sensor[i] = 0
                            else:
                                if sensor[i-1] >= sensor[i] and sensor[i-2] <= sensor[i]:
                                    # Prev value is bigger than current, but 2 prev is smaller, so adjust the prev point between the current and 2 behind
                                    sensor[i-1] = random.randint(sensor[i-2], sensor[i])
                                if sensor[i-1] >= sensor[i] and sensor[i-2] >= sensor[i]:
                                    # Prev value is bigger than current, but 2 prev is bigger, so adjust the prev between the 2 behind and max, then the current between the prev and max. 
                                    sensor[i-1] = random.randint(sensor[i-2], 200)
                                    sensor[i] = random.randint(sensor[i-1], 200)
                                else:
                                    continue
                    newMember.extend(sensor)

                Z[index] = newMember
                # Send to drivers
                DriverData[index].sendUpdatedParams(Z[index].tolist())
        
        rospy.logwarn("Next Population: {}".format(Z))
        # Clear reset
        sendResetMsg(0)        
        # Set population back to function and return it to the GA
        pop.set("X",Z)
        return pop

class MyProblem(ElementwiseProblem):

    def __init__(self, a, b, Drivers, alpha=1, beta=1):
        super().__init__(n_var=18,
                         xl=a,
                         xu=b)
        self._NumOfDrivers = Drivers
        self.alpha = alpha
        self.beta = beta

        self._GLOBALTIMEOUT = 60 * 8 # 8 minute timeout. 
        self._DRIVERFINISHEDTIMEOUT = 80 # 40 second lap * 2 laps. To allow some drivers to compelte their lap. 
        self.initTime = time.time() # Grab time before holding GA
        
        self.holdFlag = True
        self.curDriverIndex = 0
        self.driverFinished = False

    def _evaluate(self, x, out, *args, **kwargs):
        global DriverData
        
        start = 0
        currentTime = 0

        while(self.holdFlag):
            # Check conditions
            for driver in DriverData:
                if (len(driver.getDriverLapTimes()) >= NUM_LAPS) and (self.driverFinished == False):
                    start = time.time()
                    self.driverFinished = True
            currentTime = time.time()
            if ( ((self.driverFinished == True) and (currentTime > (start + self._DRIVERFINISHEDTIMEOUT))) or   # Driver finished, start a 2 minute time out for the other drivers 
                 (currentTime > (self.initTime + self._GLOBALTIMEOUT)) ):                                       # No drivers finished, check for global timeout. 
                self.holdFlag = False

        damage = DriverData[self.curDriverIndex].getDriverDamage()
        lapTimes = DriverData[self.curDriverIndex].getDriverLapTimes()
        fitvalue = self.fitness_function1(damage, lapTimes)
        rospy.loginfo("Driver: {} Fitness: {} | Damage: {} LapTimes: {}".format(self.curDriverIndex, fitvalue, damage, lapTimes))
        out["F"] = fitvalue
        self.curDriverIndex = self.curDriverIndex + 1

        # Once we analyze the 10th car, set flag back to true
        if self.curDriverIndex == (len(DriverData)):
            self.curDriverIndex = 0
            self.holdFlag = True
            self.driverFinished = False
            sendResetMsg(1)
            rospy.sleep(1) # Wait 40ms to ensure reset was sent. 
            sendResetMsg(0)
            rospy.sleep(1) # Wait 40ms to ensure reset was sent. 
            self.initTime = time.time()
        rospy.logdebug("GA Eval complete for Driver: {} out of {}...".format(self.curDriverIndex, len(DriverData)))

    def fitness_function(self, speed, damage):
        # Higher better
        return statistics.mean(speed) / damage

    def fitness_function1(self, damage, lapTimes):
        # Lower better
        # Takes average laptime (mean of all laptimes)
        if len(lapTimes) > 0 :
            if len(lapTimes) > NUM_LAPS:
                limit = NUM_LAPS
            else:
                limit = len(lapTimes)
            return damage + (self.alpha * statistics.mean(lapTimes[:limit]))
        else:
            return 1000 # Since no laps were completed, heavy penalty.

    def fitness_function2(self, damage, lapTimes, topSpeed):
        # Lower better
        return damage + (self.alpha * statistics.mean(lapTimes)) + ((self.beta) * (1/topSpeed))

class TORCS_GA(object):
    # Ideas:
    # CX = .85 or 0.7
    # MR = .09 or 0.3
    def __init__(self, cr, mr, Drivers):
        self._NumOfDrivers = Drivers
        rospy.logdebug("GA Repair starting...")
        
        base = np.array([[0, 100, 100, 160, 120, 200, 0, 80, 80, 140, 100, 200, 0, 60, 60, 120, 100, 200]])
        a = np.tile(base,(self._NumOfDrivers,1))
        off = mutation(get_mutation("int_pm", eta=3.0, prob=mr), a[1:], xl=0, xu=200)
        off = np.concatenate((base, off), axis=0)

        self.ga = GA(pop_size=self._NumOfDrivers,
                     sampling=off, #get_sampling('int_random'),
                     crossover=get_crossover("int_sbx", prob=cr, eta=3.0),
                     mutation=get_mutation('int_pm', prob=mr, eta=3.0),
                     repair=GARepair(),
                     eliminate_duplicates=True)
        self.problem = MyProblem(0, 200, self._NumOfDrivers)
        self.alpha = 1
        self.beta = 1

        # Clear reset flag
        sendResetMsg(0)
        rospy.loginfo("cr: {} mr: {}".format(cr, mr))
        rospy.logdebug("GA Repair ending...")
    
    def get_GA(self):
        return self.ga

    def execute(self):
        rospy.loginfo("--- Starting GA minimzation ---")
        run = True
        while run and not rospy.is_shutdown():
            fh = open(os.path.expanduser('~/catkin_ws/src/torcs_ga') + "/results.txt", "a")
            result = minimize(self.problem, self.ga, termination=get_termination("n_gen", 50), seed=1, verbose=True, save_history=True)
            rospy.loginfo("--- Ending GA minimzation ---")
            print("Best solution found: \nX = %s\nF = %s" % (result.X, result.F))
            #fh = open("~/catkin_ws/src/torcs_ga/results.txt", "a")
            fh.writelines("--- {} ---\n".format(datetime.now()))
            fh.writelines("Best solution found: \nX = %s\nF = %s\n" % (result.X, result.F))
            fh.writelines("---\n")
            fh.close()
            run = False
				
# ===[ SUPPORTING FUNCTIONS ]===================================================
def setupResetMsg(NumOfDrivers):
    global RestartRacePub 
    RestartRacePub = rospy.Publisher('/torcs_ga_alg/RestartRace', RestartRace, queue_size=3)

def sendResetMsg(mode):
    global RestartRacePub
    global DriverData
    rospy.logdebug("Reset Mode set to {}".format(mode))
    data = RestartRace()
    data.mode = mode
    RestartRacePub.publish(data)
    for driver in DriverData:
        driver.reset()

def setupDriverData(NumOfDrivers):
    global DriverData
    DriverData = [GADriver((i+1)) for i in range(0, NumOfDrivers)]

def updateDriverParams(index, params):
    global DriverData
    DriverData[index].sendUpdatedParams(params)

# ===[ MAIN ]===================================================================
if __name__ == '__main__':
    try:
        if len(sys.argv) == 1:
            NumOfDrivers = 1
        else:
            NumOfDrivers = int(sys.argv[1])

        rospy.init_node('torcs_ga', anonymous=True, log_level=rospy.INFO)
        rospy.loginfo("GA is configured and ready...")
        rospy.loginfo("Number of DRivers: {}".format(NumOfDrivers))

        # Setup communication channels
        setupResetMsg(NumOfDrivers)
        setupDriverData(NumOfDrivers)
        
        tc = TORCS_GA(cr=0.7, mr=0.3, Drivers=NumOfDrivers)
        tc.execute()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass

# ===[ EOF ]====================================================================
