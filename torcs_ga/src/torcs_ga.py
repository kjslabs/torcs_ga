#!/usr/bin/env python3
# ==============================================================================
# ECE/CSE 848
# Torcs GA 
# Authors: Kevin Smith (smit2958@msu.edu),
#          Chris Nosowsky (nosowsky@msu.edu)
#
# This file creates an instance of the Driver and keeps it active for driving
# in the TORCS simulation.
# ==============================================================================

# ===[ IMPORTS ]================================================================
import rospy
import sys
from torcs_ga_driver.Driver import TORCS_Driver

# ===[ MAIN ]===================================================================
if __name__ == '__main__':
    try:
        # If no argument is provided, assume 1 driver and set the port ID to 3001
        if len(sys.argv) == 1:
            PortId = 3001 
        else:
            # Get the ID that is passed in to assign to the driver (1 to 10)
            PortId = int(sys.argv[1])

        # Setup ROS node for this driver
        rospy.init_node('torcs_ga' + str(PortId), anonymous=True, log_level=rospy.INFO)
        rospy.loginfo("Driver interface {} is up and running...".format(PortId))
        
        driver = TORCS_Driver(PortId)
        driver.execute() # Endless loop while the simulation is running 
        
        rospy.spin() # Keep the node active 
    except rospy.ROSInterruptException:
        pass

# ===[ EOF ]====================================================================